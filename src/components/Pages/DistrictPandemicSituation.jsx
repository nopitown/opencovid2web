import React from 'react';

const DistricPandemicSituation = () => {
    return (
        <main className="district-pandemic-situation">
            <iframe src="https://maps.amigocloud.com/api/v1/maps/1174/view" width="100%" height="600px"></iframe>
        </main>
    )
}

export default DistricPandemicSituation;