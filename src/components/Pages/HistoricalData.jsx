import React from 'react';

const HistoricalData = () => {
    return (
        <iframe
            width="100%"
            src="https://datastudio.google.com/embed/reporting/0c5059be-0dbd-4a90-a04e-83b9a460b928/page/9xzBC"
            frameborder="0"
            style={{border:0, height: '100vh'}}
            allowfullscreen>
            </iframe>
    )
}

export default HistoricalData;